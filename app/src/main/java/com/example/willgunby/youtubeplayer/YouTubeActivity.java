package com.example.willgunby.youtubeplayer;

import android.os.Bundle;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import static com.google.android.youtube.player.YouTubePlayer.*;

/**
 * Created by willgunby on 21/03/15.
 *
 */
public class YouTubeActivity extends YouTubeBaseActivity implements OnInitializedListener {


    @Override
    public void onInitializationSuccess(Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        youTubePlayer.setPlayerStateChangeListener(playerStateChangeListener);
        youTubePlayer.setPlaybackEventListener(playbackEventListener);
        if(!wasRestored) {
            youTubePlayer.cueVideo(Consts.YOUTUBE_VIDEO_ID);
        }
    }

    @Override
    public void onInitializationFailure(Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Cannot initialise player - "+ youTubeInitializationResult.name(), Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube);
        YouTubePlayerView player = (YouTubePlayerView) findViewById(R.id.youtube_player);
        //if (!player.isInEditMode()) {
            player.initialize(Consts.GOOGLE_API_KEY, this);
        //}
    }



    private PlayerStateChangeListener playerStateChangeListener = new PlayerStateChangeListener(){
        @Override        public void onLoading()                        {        }
        @Override        public void onLoaded(String s)                 {        }
        @Override        public void onAdStarted()                      {        }
        @Override        public void onVideoStarted()                   {        }
        @Override        public void onVideoEnded()                     {        }
        @Override        public void onError(ErrorReason errorReason)   {        }
    };

    private PlaybackEventListener playbackEventListener = new PlaybackEventListener(){
        @Override        public void onPlaying()            {        }
        @Override        public void onPaused()             {        }
        @Override        public void onStopped()            {        }
        @Override        public void onBuffering(boolean b) {        }
        @Override        public void onSeekTo(int i)        {        }
    };

}
