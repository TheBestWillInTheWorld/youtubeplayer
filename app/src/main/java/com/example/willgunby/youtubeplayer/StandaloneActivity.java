package com.example.willgunby.youtubeplayer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.youtube.player.YouTubeStandalonePlayer;


/**
 * Created by willgunby on 21/03/15.
 *
 */
public class StandaloneActivity extends Activity implements View.OnClickListener {

    Button btnPlay;
    Button btnPlaylist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_standalone);

        btnPlay = (Button) findViewById(R.id.btnPlay);
        btnPlaylist = (Button) findViewById(R.id.btnPlaylist);
        btnPlay.setOnClickListener      (this);
        btnPlaylist.setOnClickListener  (this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        if (v == btnPlay) {
            //play video
            intent = YouTubeStandalonePlayer.createVideoIntent(this, Consts.GOOGLE_API_KEY, Consts.YOUTUBE_VIDEO_ID);
        }
        else if (v == btnPlaylist) {
            //play video
            intent = YouTubeStandalonePlayer.createPlaylistIntent(this, Consts.GOOGLE_API_KEY, Consts.YOUTUBE_PLAYLIST_ID);
        }
        if (intent != null) {
            startActivityForResult(intent, 0);
        }
    }


}
