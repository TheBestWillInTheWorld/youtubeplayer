package com.example.willgunby.youtubeplayer;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class MainActivity extends ActionBarActivity {

    Button btnPlay;
    Button btnSubMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnPlay = (Button) findViewById(R.id.btnPlay);
        btnSubMenu = (Button) findViewById(R.id.btnSubMenu);
        btnPlay.setOnClickListener(new View.OnClickListener()       { @Override public void onClick(View v) { btnPlay_Click(v); } });
        btnSubMenu.setOnClickListener(new View.OnClickListener()    { @Override public void onClick(View v) { btnSubMenu_Click(v); } });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void btnPlay_Click(View v) {
        Intent intent = new Intent(this, YouTubeActivity.class);
        startActivity(intent);
    }
    public void btnSubMenu_Click(View v) {
        Intent intent = new Intent(this, StandaloneActivity.class);
        startActivity(intent);
    }
}
